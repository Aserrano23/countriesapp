import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from '../interfaces/pais.interface';

@Injectable({
  providedIn: 'root'
})
export class PaisService {

  private _url: string = 'https://restcountries.eu/rest/v2';

  get params() {
    return new HttpParams().set('fields', 'name;capital;flag;population;numericCode;alpha3Code;translations;alpha2Code')
  }

  constructor(private http: HttpClient) { }

  BuscarPais(termino: string): Observable<Country[]>{

    const url = `${this._url}/name/${termino}`

    return this.http.get<Country[]>( url, {params: this.params} );
  }

  BuscarCapital(termino: string): Observable<Country[]>{

    const url = `${this._url}/capital/${termino}`

    return this.http.get<Country[]>( url, {params: this.params} );
  }

  mostrarPais(id: string): Observable<Country>{

    const url = `${this._url}/alpha/${id}`

    return this.http.get<Country>( url, {params: this.params} );
  }

  BuscarPorRegion(termino: string): Observable<Country[]>{

    const url = `${this._url}/region/${termino}`

    return this.http.get<Country[]>( url, {params: this.params} );
  }
}
