import { Component } from '@angular/core';
import { Country } from '../../interfaces/pais.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-por-capital',
  templateUrl: './por-capital.component.html',
  styles: [
  ]
})
export class PorCapitalComponent {

  termino: string = ''
  hayError: boolean = false;
  _paises: Country[] = [];

  constructor(private paisService: PaisService){}

  Buscar(termino: string){

    this.hayError = false;
    this.termino = termino;

    this.paisService.BuscarCapital( termino )
      .subscribe( paises => {
        
        this._paises = paises;

      }, err => {

        this.hayError = true;
        this._paises = [];
        
      })

  }
}
