import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';
import { Country } from '../../interfaces/pais.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-ver-pais',
  templateUrl: './ver-pais.component.html',
  styles: [
  ]
})
export class VerPaisComponent implements OnInit {

  private _url: string = 'https://restcountries.eu/rest/v2/';
  pais!: Country;

  constructor(private activatedRoute: ActivatedRoute,private paisService:PaisService) { }

  ngOnInit(): void {


    this.activatedRoute.params
      .pipe(
        switchMap(param => this.paisService.mostrarPais(param.id)))
        
      .subscribe(pais => {
        this.pais = pais;
      })

    // this.activatedRoute.params
    //   .subscribe( params => {

    //     this.paisService.mostrarPais(params.id)
    //       .subscribe( pais => {

    //         this.pais = pais;
    //       })
    //   })
  }
}
