import { Component } from '@angular/core';
import { PaisService } from '../../services/pais.service';
import { Country } from '../../interfaces/pais.interface';

@Component({
  selector: 'app-por-pais',
  templateUrl: './por-pais.component.html',
  styles: [`
    li{
      cursor: pointer;
    }
  `
  ]
})
export class PorPaisComponent {

  termino: string = ''
  hayError: boolean = false;
  _paises: Country[] = [];

  paisesSugeridos: Country[] = [];
  mostrarSugerencia: boolean = false;

  constructor(private paisService: PaisService){}

  Buscar(termino: string){

    this.hayError = false;
    this.termino = termino;
    this.mostrarSugerencia = false;
    
    this.paisService.BuscarPais( termino )
      .subscribe( paises => {

        console.log(paises);
        this._paises = paises;

      }, err => {

        this.hayError = true;
        this._paises = [];
        
      })

  }

  sugerencias(termino: string){
    this.hayError = false;
    this.termino = termino;
    this.mostrarSugerencia = true;

    this.paisService.BuscarPais(termino)
      .subscribe( 
        paises =>this.paisesSugeridos = paises.splice(0,5),
        (err) => this.paisesSugeridos = []
      );

  }

  buscarSugerido(termino: string){
    this.Buscar(termino);
  }

}
